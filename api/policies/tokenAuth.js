global.Buffer = global.Buffer || require('buffer').Buffer;

if (typeof btoa === 'undefined') {
  global.btoa = function (str) {
    return new Buffer(str).toString('base64');
  };
}

if (typeof atob === 'undefined') {
  global.atob = function (b64Encoded) {
    return new Buffer(b64Encoded, 'base64').toString();
  };
}

module.exports = function(req, res, next) {
  var token;

  if (req.headers && req.headers.authorization) {
   var parts = req.headers.authorization.split(' ');
   if (parts.length == 2) {
     var scheme = parts[0],
       credentials = parts[1];

       var userObj = credentials.split('.')[1];


       SessionUsers.findOne({id: JSON.parse(atob(userObj)).sessionId}).exec(function(err, user){
         if(!user){
           return res.json('401', {"err": "Session Is not Valid"});
         }
       });


     if (/^Bearer$/i.test(scheme)) {
       token = credentials;
     }
   } else {
     return res.json(401, {err: 'Format is Authorization: Bearer [token]'});
   }
  } else if (req.param('token')) {
   token = req.param('token');
   // We delete the token from param to not mess with blueprints
   delete req.query.token;
  } else {
   return res.json(401, {err: 'No Authorization header was found'});
  }

  sailsTokenAuth.verifyToken(token, function(err, token) {

    if (err) return res.json(401, {err: 'The token is not valid'});

    req.token = token;

    next();
  });
};
