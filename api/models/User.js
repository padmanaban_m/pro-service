module.exports = {

    attributes: {

        name: {
            type: 'string',
            required: true
        },

        sessionId: {
            type: 'string',
            unique: 'true'
        },

        emailId: {
            type: 'string',
            email: true,
            required: true,
            unique: true
        },

        phoneNumber: {
            type: 'string',
            unique: true
        },

        encryptedPassword: {
            type: 'string',
            required: true
        },

        lastLoggedIn: {
            type: 'date',
            required: true,
            defaultsTo: new Date(0)
        }

    }
};