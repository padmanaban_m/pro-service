module.exports = {

    attributes: {

        userId: {
            type: 'string',
            required: true
        },

        name: {
            type: 'string',
            required: true
        }
    }
};