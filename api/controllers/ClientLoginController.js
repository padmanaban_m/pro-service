/**
 * ClientLoginController
 *
 * @description :: Server-side logic for managing Clientlogins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    login: function (req, res) {
        // Try to look up user using the provided email address
        User.findOne({ or: [{ emailId: req.param('emailId') }, { userId: req.param('emailId') }] }).exec(function (err, user) {

            if (err) { return res.negotiate(err); }
            if (!user) { return res.notFound({ err: "User Doesn't Exists!!" }); }
            // Compare password attempt from the form params to the encrypted password
            // from the database (`user.password`)
            require('machinepack-passwords').checkPassword({
                passwordAttempt: req.param('password'),
                encryptedPassword: user.encryptedPassword
            }).exec({
                error: function (err) {
                    return res.negotiate(err);
                },
                // If the password from the form params doesn't checkout w/ the encrypted
                // password from the database...
                incorrect: function () {
                    return res.notFound({ err: "Email Id or Password Doesn't Match!" });
                },
                success: function () {
                    SessionUsers.create({
                        userId: user.userId,
                        emailId: user.emailId,
                        name: user.name
                    }).exec(function (err, records) {
                        user.sessionId = records.id;
                        let userObject = sailsTokenAuth.createPayload(user);
                        return res.json({ user: user, token: sailsTokenAuth.issueToken(userObject) });
                    });
                }
            });
        });
    },

    /**
     * Sign up for a user account.
     */
    signup: function (req, res) {
        User.findOne({ or: [{ emailId: req.param('emailId') }, { userId: req.param('userId') }] }).exec(function (err, record) {
            if (record) {
                return res.json('409', { err: "User Already Exists!!" })
            }
            else {
                var Passwords = require('machinepack-passwords');
                // Encrypt a string using the BCrypt algorithm.
                Passwords.encryptPassword({
                    password: req.param('password'),
                    difficulty: 10,
                }).exec({
                    // An unexpected error occurred.
                    error: function (err) {
                        return res.negotiate(err);
                    },
                    // OK.
                    success: function (encryptedPassword) {
                        User.create({
                            name: req.param('name'),
                            userId: req.param('userId'),
                            emailId: req.param('emailId'),
                            phoneNumber: req.param('phoneNumber'),
                            encryptedPassword: encryptedPassword,
                            lastLoggedIn: new Date(),
                        }, function userCreated(err, newUser) {
                            if (err) {
                                return res.negotiate(err);
                            }
                            SessionUsers.create({
                                userId: newUser.userId,
                                emailId: newUser.emailId,
                                name: newUser.name
                            }).exec(function (err, records) {
                                newUser.sessionId = records.id;
                                let userObject = sailsTokenAuth.createPayload(newUser);
                                return res.json({ user: newUser, token: sailsTokenAuth.issueToken(userObject) });
                            });
                        });
                    }
                });
            }
        });
    },

    logout: function (req, res) {
        User.findOne({ or: [{ emailId: req.param('emailId') }, { userId: req.param('emailId') }] }).exec(function (err, user) {
            if (err) { res.negotiate(err); }
            // If session refers to a user who no longer exists, still allow logout.
            if (!user) {
                sails.log.verbose('Session refers to a user who no longer exists.');
                return res.notFound({ err: "Session doesn't Exist" });
            }
            SessionUsers.findOne({ id: req.param('sessionId') }).exec(function (err, record) {
                SessionUsers.destroy({ id: req.param('sessionId') }).exec(function (err, record) {
                    if (!err) {
                        return res.ok();
                    }
                });
            });

            // Either send a 200 OK or redirect to the home page
            return res.ok();

        });
    }
};


