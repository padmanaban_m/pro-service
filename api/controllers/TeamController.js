/**
 * TeamController
 *
 * @description :: Server-side logic for managing teams
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    'create': function (req, res) {
        Team.create(req.params.all(), function (err, team) {
            if (!err && team) {
                return res.json('201', team.id);
            } else {
                return res.badRequest();
            }
        });
    },
    'find': function (req, res) {
        var id = req.param('id');

        if (id) {
            Team.findOne(id, function (err, team) {
                if (!err && team) {
                    return res.json('200', team);
                }
                else {
                    return res.notFound();
                }
            });

        } else {
            Team.find(function (err, team) {
                if (!err && team) {
                    return res.json('200', team);
                }
                else {
                    return res.badRequest();
                }
            });
        }
    },
    'update': function (req, res) {
        Team.update(req.param('id'), req.body, function (err, team) {
            if (!err && team) {
                return res.json('200', team);
            } else {
                return res.notFound();
            }
        });

    },
    'destroy': function (req, res) {
        Team.destroy(req.param('id'), function (err, team) {

            if (!err && team && team.length >= 1) {
                return res.json('200', team);
            } else {
                return res.badRequest();
            }
        });
    }
};

