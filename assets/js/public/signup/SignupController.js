angular.module('SignupModule').controller('SignupController', ['$scope', '$http', 'toastr', function ($scope, $http, toastr) {

    // set-up loading state
    $scope.signupForm = {
        loading: false
    }

    $scope.submitSignupForm = function () {

        // Set the loading state (i.e. show loading spinner)
        $scope.signupForm.loading = true;

        // Submit request to Sails.
        $http.post('/signup', {
            name: $scope.signupForm.name,
            title: $scope.signupForm.title,
            emailId: $scope.signupForm.emailId,
            password: $scope.signupForm.password
        })
            .then(function onSuccess(sailsResponse) {
                window.location = '/';
            })
            .catch(function onError(sailsResponse) {

                // Handle known error type(s).
                // If using sails-disk adpater -- Handle Duplicate Key
                var emailIdAddressAlreadyInUse = sailsResponse.status == 409;

                if (emailIdAddressAlreadyInUse) {
                    toastr.error('That emailId address has already been taken, please try again.', 'Error');
                    return;
                }

            })
            .finally(function eitherWay() {
                $scope.signupForm.loading = false;
            })
    }

    $scope.submitLoginForm = function () {

        // Set the loading state (i.e. show loading spinner)
        $scope.loginForm.loading = true;

        // Submit request to Sails.
        $http.put('/login', {
            emailId: $scope.loginForm.emailId,
            password: $scope.loginForm.password
        })
            .then(function onSuccess() {
                // Refresh the page now that we've been logged in.
                window.location = '/';
            })
            .catch(function onError(sailsResponse) {

                // Handle known error type(s).
                // Invalid username / password combination.
                if (sailsResponse.status === 400 || 404) {
                    // $scope.loginForm.topLevelErrorMessage = 'Invalid emailId/password combination.';
                    //
                    toastr.error('Invalid emailId/password combination.', 'Error', {
                        closeButton: true
                    });
                    return;
                }

                toastr.error('An unexpected error occurred, please try again.', 'Error', {
                    closeButton: true
                });
                return;

            })
            .finally(function eitherWay() {
                $scope.loginForm.loading = false;
            });
    };

}]);
